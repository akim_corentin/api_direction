package Modele;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ExceptionPersonnalisee extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExceptionPersonnalisee(String string, String message) {
		super();
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Erreur");
		alert.setHeaderText(string);
		alert.setContentText(message);

		alert.showAndWait();
	}

}
