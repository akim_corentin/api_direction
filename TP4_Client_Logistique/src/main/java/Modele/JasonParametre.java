package Modele;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

public class JasonParametre {
	
	private List<String> locations;

	public JasonParametre(List<String> locations) {
		super();
		this.locations = locations;
	}

	public List<String> getLocations() {
		return locations;
	}

	public void setLocations(List<String> locations) {
		this.locations = locations;
	}
	
	
	
	public static void main(String[] args) {
		List<String> locations = new ArrayList<String>();
		locations.add("235 Rue Fortin, Quebec, QC G1M 3M2");
		locations.add("255 Ch Ste-Foy, Quebec, QC G1R 1T5");
		locations.add("707 Boulevard Charest O, Quebec, QC G1N 4P6");
		locations.add("860 Boulevard Charest E, Quebec, QC G1K 8S5");
		
		JasonParametre jasonParametre = new JasonParametre(locations);
		
		Gson gson = new Gson();
		System.out.println(gson.toJson(jasonParametre));
		
	}

}
