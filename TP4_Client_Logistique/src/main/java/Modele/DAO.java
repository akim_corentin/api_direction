package Modele;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

//import Modele.package_logger.MyLoggingMessageErreur;

/**
 * Classe abstraite DAO generique.
 * @author akimsoule
 *
 * @param <T>
 * l'objet de la base de donnees.
 */
public abstract class DAO<T> {
	protected Connection connect = null;

	public DAO(Connection conn) {
		this.connect = conn;
	}

	/**
	 * Methode permettant de lire la BD.
	 * @param sql
	 * La requette sql.
	 * @return
	 * le resultat de la requette.
	 */
	protected ResultSet LireBD(String sql) {
		ResultSet resultSet = null;
		Statement statement = null;
		try {
			statement = this.connect.createStatement();
			resultSet = statement.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			//MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		return resultSet;
	}

	/**
	 * Methode permettant d'executer une requette.
	 * @param sql
	 * la requette sql.
	 * @param list
	 * la liste des objets a inserer dans la requette.
	 * @return
	 * vrai si la requette s'est bien deroulee et faux dans le cas contraire.
	 */
	protected boolean executerRequette(String sql, List<Object> list) {
		boolean ok = false;
		PreparedStatement preparedStatement = null;

		try {
			preparedStatement = this.connect.prepareStatement(sql);

			for (int i = 0; i < list.size(); i++) {
				preparedStatement.setObject(i + 1, list.get(i));
			}
			preparedStatement.executeUpdate();
			ok = true;
		} catch (SQLException e) {
			ok = false;
			e.printStackTrace();
			//MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		
		return ok;

	}

}
