package Modele;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;

public class Utilitaire {

	private static HttpURLConnection con;

	public List<Client> lireClient() {
		List<Client> list = DAOFactory.getClientDAO().getAll();
		return list;

	}

	public void appelleApi(Map<Integer, String> map) {
		System.out.println(map);
		List<String> listAdresse = new ArrayList<String>();
		Iterator<String> it = map.values().iterator();
		while (it.hasNext()) {
			String adresse = it.next();
			listAdresse.add(adresse);
		}
		JasonParametre jasonParametre = new JasonParametre(listAdresse);
		Gson gson = new Gson();
		try {
			LinkedList<Integer> set = getSeQuence(gson.toJson(jasonParametre));
			System.out.println(set);
			String route = "";
			String cle = "";
			String aAfficher = "";
			aAfficher += "l'ordre est \n";
			for (int i = 0; i < listAdresse.size(); i++) {
				if(getKey(map, listAdresse.get(set.get(i))) != 0) {
					route += getKey(map, listAdresse.get(set.get(i)));
				}
					
				aAfficher += (i + 1) + "-" + getKey(map, listAdresse.get(set.get(i))) + "\n";
			}
			System.out.println(aAfficher);
			System.out.println("la cle est " + map.keySet());
			System.out.println("la route est "+route);
			DAOFactory.getRouteDAO().insertIntoRoute(map.keySet().toString(), route);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// DAOFactory.getRouteDAO().insertIntoRoute(cle, route)

	}

	private static LinkedList<Integer> getSeQuence(String json) throws IOException {
		LinkedList<Integer> nombre;
		json = json.replaceAll(" ", "%20");
		String url = "https://www.mapquestapi.com/directions/v2/optimizedRoute?json=" + json
				+ "&outFormat=json&key=zQmvLzL95GwQ07vnXTLwAu4lHVCVbj7i";
		try {

			URL myurl;

			myurl = new URL(url);

			con = (HttpURLConnection) myurl.openConnection();

			con.setDoOutput(true);
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", "Java client");
			con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
				// wr.write(postData);
			}

			StringBuilder content;

			try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {

				String line;
				content = new StringBuilder();

				while ((line = in.readLine()) != null) {
					content.append(line);
					content.append(System.lineSeparator());
				}
			}

			String reponse = (content.toString());
			// System.out.println(reponse);

			int position = reponse.indexOf("locationSequence\":[");

			// System.out.println(reponse.charAt(position + 19));
			int depart = 19;
			boolean ok = true;

			nombre = new LinkedList<Integer>();

			while (ok && depart <= 40) {

				if (Character.isDigit(reponse.charAt(position + depart))) {
					nombre.add(Character.getNumericValue(reponse.charAt(position + depart)));
				}
				++depart;

			}

		} finally {

			con.disconnect();
		}

		return nombre;
	}

	public Integer getKey(Map<Integer, String> map, String value) {
		Integer retour = null;
		for (Entry<Integer, String> entry : map.entrySet()) {
			if (entry.getValue().equals(value)) {
				retour = entry.getKey();
				break;
			}
		}
		return retour;

	}

}
