package Modele;

import java.sql.Connection;

/**
 * Classe permettant de recuperer des objets servant a interagir avec la bd.
 * @author akimsoule
 *
 */
public class DAOFactory {
	protected static final Connection conn = ConnectionModele.getInstance();

	/**
	 * @return
	 * l'objet ClientDAO.
	 */
	public static ClientDAO getClientDAO() {
		return new ClientDAO(conn);
	}
	
	public static RouteDAO getRouteDAO() {
		return new RouteDAO(conn);
	}
}
