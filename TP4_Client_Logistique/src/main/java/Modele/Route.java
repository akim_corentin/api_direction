package Modele;

public class Route {
	
	private int idRoute;
	private String cle;
	private String routeVarChar;
	
	public Route(int idRoute, String cle, String routeVarChar) {
		super();
		this.idRoute = idRoute;
		this.cle = cle;
		this.routeVarChar = routeVarChar;
	}

	public int getIdRoute() {
		return idRoute;
	}

	public void setIdRoute(int idRoute) {
		this.idRoute = idRoute;
	}

	public String getCle() {
		return cle;
	}

	public void setCle(String cle) {
		this.cle = cle;
	}

	public String getRouteVarChar() {
		return routeVarChar;
	}

	public void setRouteVarChar(String routeVarChar) {
		this.routeVarChar = routeVarChar;
	}

	@Override
	public String toString() {
		return "Route [idRoute=" + idRoute + ", cle=" + cle + ", routeVarChar=" + routeVarChar + "]";
	}
	
	
	

}
