package Modele;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RouteDAO extends DAO<Route> {

	public RouteDAO(Connection conn) {
		super(conn);
	}
	
	public List<Route>getAll(){
		List<Route> list = new ArrayList<Route>();
		String sql = "select * from route";
		ResultSet resultSet = LireBD(sql);
		
		try {
			while (resultSet.next()) {
				list.add(new Route(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3)));
			}
		} catch (SQLException e) {

			e.printStackTrace();
			//MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		return list;
		
	}
	
	public boolean insertIntoRoute(String cle, String route) {
		boolean retour = false;
		String sql = "select * from route where cle = '"+cle+"'";
		ResultSet resultSet = LireBD(sql);
		int nombre = 0;
		try {
			while (resultSet.next()) {
				++nombre;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		
		if(nombre == 0) {
			List<Object> list = new ArrayList<Object>();
			list.add(cle);
			list.add(route);
			String sql2 = "insert into route(cle, routeVarchar) values (?, ?)";
			executerRequette(sql2, list);
			retour = true;
			
			
		}else {
			retour = false;
		}
		return retour;
		
	}

}
