package Modele;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientDAO extends DAO<Client> {

	public ClientDAO(Connection conn) {
		super(conn);
	}
	
	public List<Client>getAll(){
		List<Client> list = new ArrayList<Client>();
		String sql = "select * from client";
		ResultSet resultSet = LireBD(sql);
		
		try {
			while (resultSet.next()) {
				list.add(new Client(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3)));
			}
		} catch (SQLException e) {

			e.printStackTrace();
			//MyLoggingMessageErreur.log(Level.WARNING, e.getMessage());
		}
		return list;
		
	}

}
