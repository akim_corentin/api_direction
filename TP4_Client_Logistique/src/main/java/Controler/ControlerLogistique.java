package Controler;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import Modele.Client;
import Modele.ExceptionPersonnalisee;
import Modele.Utilitaire;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

public class ControlerLogistique implements Initializable {
	
	Utilitaire utilitaire;
	List<Client> listClient;

    @FXML
    private Button boutonGenereId;

    @FXML
    private CheckBox checkBoxClient1Id;

    @FXML
    private CheckBox checkBoxClient2Id;

    @FXML
    private CheckBox checkBoxClient3Id;

    @FXML
    private CheckBox checkBoxClient4Id;

    @FXML
    private CheckBox checkBoxClient5Id;

    @FXML
    private TextField textFieldOrigineDestinationId;
    
    @FXML
    void genererOnAction(ActionEvent event) {
    	int nombreDeClient = 0;
    	Map<Integer, String> map = new HashMap<Integer, String>();
    	String cle = "";
    	List<String> listAdresse = new ArrayList<String>();
    	map.put(0, textFieldOrigineDestinationId.getText().split("--")[1]);
    	listAdresse.add(textFieldOrigineDestinationId.getText().split("--")[1]);
    	if(checkBoxClient1Id.isSelected()) {
    		++nombreDeClient;
    		cle +="1";
    		listAdresse.add(checkBoxClient1Id.getText().split("--")[1]);
    		map.put(1, checkBoxClient1Id.getText().split("--")[1]);
    	}
    	if(checkBoxClient2Id.isSelected()) {
    		++nombreDeClient;
    		cle +="2";
    		listAdresse.add(checkBoxClient2Id.getText().split("--")[1]);
    		map.put(2, checkBoxClient2Id.getText().split("--")[1]);
    		
    	}
    	if(checkBoxClient3Id.isSelected()) {
    		++nombreDeClient;
    		cle +="3";
    		listAdresse.add(checkBoxClient3Id.getText().split("--")[1]);
    		map.put(3, checkBoxClient3Id.getText().split("--")[1]);
    		
    	}
    	if(checkBoxClient4Id.isSelected()) {
    		++nombreDeClient;
    		cle +="4";
    		listAdresse.add(checkBoxClient4Id.getText().split("--")[1]);
    		map.put(4, checkBoxClient4Id.getText().split("--")[1]);
    		
    	}
    	if(checkBoxClient5Id.isSelected()) {
    		++nombreDeClient;
    		cle +="5";
    		listAdresse.add(checkBoxClient5Id.getText().split("--")[1]);
    		map.put(5, checkBoxClient5Id.getText().split("--")[1]);
    	}
    	
    	if (nombreDeClient < 2) {
    		new ExceptionPersonnalisee("Erreur", "Vous devez selectionne au moins 2 clients pour faire appel a l'API");
    	}else {
    		map.put(6, textFieldOrigineDestinationId.getText().split("--")[1]);
    		listAdresse.add(textFieldOrigineDestinationId.getText().split("--")[1]);
    		utilitaire.appelleApi(map);
    	}
    }

	public void initialize(URL location, ResourceBundle resources) {
		utilitaire = new Utilitaire();
		listClient = utilitaire.lireClient();
		checkBoxClient1Id.setText(listClient.get(0).getNomClient()+"--"+listClient.get(0).getAdresse());
		checkBoxClient2Id.setText(listClient.get(1).getNomClient()+"--"+listClient.get(1).getAdresse());
		checkBoxClient3Id.setText(listClient.get(2).getNomClient()+"--"+listClient.get(2).getAdresse());
		checkBoxClient4Id.setText(listClient.get(3).getNomClient()+"--"+listClient.get(3).getAdresse());
		checkBoxClient5Id.setText(listClient.get(4).getNomClient()+"--"+listClient.get(4).getAdresse());
		textFieldOrigineDestinationId.setText(listClient.get(5).getNomClient()+"--"+listClient.get(5).getAdresse());
		textFieldOrigineDestinationId.setEditable(false);
		//System.out.println(utilitaire.lireClient());
		
	}
    
    

}
